import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class WordSearch {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		WordSearchGrid wsg = new WordSearchGrid();
		int count = wsg.count;
		int hint = 0;
		wsg.placeWords();
		System.out.println("This puzzle contains " + count + " five-letter words.");
		System.out.println("They are horizontal left to right.");
		System.out.println("They are vertical top to bottom.\n");

		System.out.println(wsg); // toString

		List<String> wordsList = new ArrayList<String>(Arrays.asList(wsg.wordsArr));
		System.out.println(wordsList);

		Scanner scan = new Scanner(System.in);

		while (count != 0) {
			System.out.println(count + " words left.");
			System.out.println("Enter a 5 letter word: ");
			String s = scan.next().toUpperCase();

			if (wordsList.contains(s)) {
				count -= 1;
				System.out.println("You found " + s + "!\n");
			} else {
				System.out.println(s + " not found. Please try again.\n");
				hint += 1;
			}

			if (hint > 5) {
				System.out.println(wordsList);
			}
		}

		System.out.println("Congrats you have found all the words!");
		System.out.println(wsg.revealAnswer());
	}

}
